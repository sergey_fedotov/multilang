#pragma once

#if ARDUINO >= 100
#include <Arduino.h>
#else
#include <WProgram.h>
#endif

#include <vector>

//#define DEBUG_ML true


class Lang {
private:
    uint8_t _lang;
    std::vector <PGM_P  const *> msgsPointers;


public:
  Lang(){};
  ~Lang(){};
  Lang* addLang(PGM_P const * lang){
    //static uint8_t langs = 0;
    msgsPointers.push_back(lang);
    //langs ++;
    return this;
  };

  Lang(const uint8_t langsQty, PGM_P const ** langs){
    assert ( langsQty > 0 );
    uint8_t index = 0;
    while (index < langsQty)
    { 
      msgsPointers.push_back(langs[index]);
#if DEBUG_ML
      Serial.print(__PRETTY_FUNCTION__); Serial.print(F(" ["));
      Serial.print(index); Serial.print(F("] "));
      Serial.println(langs[index][0]);
#endif     
      index++; 
    }
    setCurrent(0);
    #if DEBUG_ML
          Serial.print(F("[DEBUG_ML]: ")); 
          Serial.print(msgsPointers.size());
          Serial.println(F(" languages setted"));
          
  #endif
  };
  Lang(const uint8_t langsQty, ...) 
  {
//  	if ( langsQty > 0 ) 
    assert( langsQty > 0  );
  	{
  		uint8_t n = langsQty;
  		va_list p;
  	  	va_start( p, langsQty);
  	  	while (n--)
  	  	{
  	  		
  	  		msgsPointers.push_back( va_arg( p, PGM_P  const *));
#if DEBUG_ML
  	  		uint8_t index = msgsPointers.size()-1;
  	  		Serial.print(F("[DEBUG_ML]: Add msgs "));
  	  		Serial.print(F("["));
  	  		Serial.print(index);
  	  		Serial.print(F("] "));
  	  		
  	  		Serial.println(msgsPointers.at(index)[0]);
#endif
  	  	}
  	  	va_end( p );
  	    setCurrent(0);
#if DEBUG_ML
          Serial.print(F("[DEBUG_ML]: ")); 
  	  		Serial.print(msgsPointers.size());
  	  		Serial.println(F(" languages setted"));
  	  		
#endif

  	}
  };


  bool setCurrent(uint8_t lang){
    bool res = lang < msgsPointers.size();
    if ( res ) {
      _lang = lang; 
      //res = true;
    }
    return res;
  };

  bool setCurrent(String lang){
    lang.trim();
    lang.toUpperCase();
    bool res = false;
    for ( uint8_t i=0; i< msgsPointers.size() ; i++){
#if DEBUG_ML
  		Serial.print(F("[DEBUG_ML]: Compare "));
  		Serial.print(msgsPointers.at(i)[0]);
	    Serial.println( F(" =? ") + lang);  	  		
#endif    	
      if ( ! strncmp( msgsPointers.at(i)[0], lang.c_str(), 2 ) ){
        return setCurrent(i);
      }
    }
    return res;
  };

  uint8_t getCurrent(){
    return _lang;
  };

  String toString(int8_t l = -1){
   	if ( l < 0 )
  	  return String(msgsPointers.at(_lang)[0]);
  	else if ( l < (int8_t)msgsPointers.size() )
  	  return String(msgsPointers.at(l)[0]);
    else
      return (char *)0;
  };

  String getListLanguages(String sep = ", "){
    String ls ((char *)0);
    for ( uint8_t i = 0; i< msgsPointers.size(); i++){
      ls += String(msgsPointers.at(i)[0]);
      if ( i != ( msgsPointers.size() -1) ) {
        ls += sep;
      }
    }
    return ls;
  };
/*
  PGM_P const msg(uint m){
    return msgsPointers.at(_lang)[m] ;
  };

  String msgString(uint m){
    return String( (PGM_P) msgsPointers.at(_lang)[m] );
  };
  */
  inline const char * msg(uint m){
    return msgsPointers.at(_lang)[m];
  };

  inline const char * operator[] (uint m){
    return msgsPointers.at(_lang)[m];
  };
/*  String msgStr(uint m){
    return String( msg(m));
  };
  */
};
