#pragma once

namespace LANG_EN {
	const char  langID[] PROGMEM = "EN";
	const char  langName[] PROGMEM = "English";
  	const char  test[] PROGMEM = "This is test MultiLang library by Sergey Fedotov";

  	PGM_P const PROGMEM msgs[] = { langID,
  		langName, test 
  	};
  };