#pragma once

namespace LANG_RU {
	const char  langID[] PROGMEM = "RU";
	const char  langName[] PROGMEM = "Русский";
  	const char  test[] PROGMEM = "Это тест МногоЯзычной библиотеки (MultiLang) от Сергея Федотова";

  	PGM_P const PROGMEM msgs[] = { langID,
  		langName, test 
  	};
  };