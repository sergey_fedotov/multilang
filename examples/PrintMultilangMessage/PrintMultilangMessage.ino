
#include <MultiLang.h>
#include "messages.h"

void testPrint(Lang& lang){
	Serial.print(F("Current lang: "));
	Serial.println( lang.toString());
	//Serial.println( lang.msg(MSG::languageName) + F(": ") + lang.msg(MSG::test) );
	Serial.print( lang[MSG::languageName]);
	Serial.print(F(": "));
	Serial.println( lang[MSG::test] );
}
void setup() {
	Serial.begin(9600);
  PGM_P const* langs[] = {LANG_EN::msgs, LANG_RU::msgs};
	Lang lang(2, langs );
  //Lang lang( 2, LANG_EN::msgs, LANG_RU::msgs );

	Serial.print("\nList of testing languages: ");
	Serial.println( lang.getListLanguages());

	lang.setCurrent(0); // EN, this is default value. Can be commented. 
	testPrint(lang);
	
	lang.setCurrent("ru"); // index of language (2 chars) in item 0 in LANG_XX array. case insensitive
	testPrint(lang);
}

void loop(){

}